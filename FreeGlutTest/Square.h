#pragma once

#include "stdafx.h"
#include "Shape.h"

class Square : public Shape {

private:
	const float a, b, c, d;

public:
	Square(float a, float b, float c, float d) : a(a), b(b), c(c), d(d) {};
	void render() override;

};