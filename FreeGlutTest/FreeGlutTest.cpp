﻿// FreeGlutTest.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
/* System */
#include<windows.h>
#include<stdio.h>
/* G³ówna bibloteka OpenGL */
#include<GL/gl.h>
/* Biblioteka GLUT */
#include<GL/freeglut.h>
#include <time.h>

#define SPEED 0.1


void DrawScene(void);

void DrawSceneAxes(void);

void InitOpenGL(void);

void ReshapeWindow(int width, int height);

void KeyboardFunc(unsigned char key, int x, int y);

int mainWindow;

GLfloat x_angle = 0.0;
GLfloat y_angle = 0.0;
GLfloat z_angle = 0.0;

int wybor;

/* Funkcja main */
int main(int argc, char **argv)
{

	printf("W i S aby obracać wokół OX, A i D aby orbacać w okół OY, Q i E aby obracać w okół OZ");
	printf("dowolny przycisk aby zaprzestać rotacji");

	// Inicjujemy bibliotekê GLUT
	glutInit(&argc, argv);

	// Inicjujemy: format koloru, dwa bufoy ramki
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
	// Ustawiamy pocz¹tkowe wymiary okna
	glutInitWindowSize(800, 600);
	// Ustawiamy pozycjê okna - lewy górny naro¿nik
	glutInitWindowPosition(850, 100); //bylo (150,150) !!!!!!!!!!!!!!!!!!!!!!!!
									  // Tworzymy g³ówne okno programu
	mainWindow = glutCreateWindow("Transformacje czajnikowe");

	// Sprawdzamy powodzenie operacji
	if (mainWindow == 0) {
		puts("Nie mozna stworzyc okna!!!\nWyjscie z programu.\n");
		exit(-1);
	}
	// Czynimy aktywnym okno g³ówne programu
	glutSetWindow(mainWindow);

	// Tutaj rejestrujemy funkcje narzêdziowe - tzw. callbacks
	glutDisplayFunc(DrawScene);
	glutReshapeFunc(ReshapeWindow);

	// Ustawienia pocz¹tkowe
	InitOpenGL();

	// W³¹czamy mechanizm usuwania niewidocznych powierzchni
	glEnable(GL_DEPTH_TEST);
	glutKeyboardFunc(KeyboardFunc);
	// Wejœcie do pêtli programu
	glutMainLoop();



	return(0);
}


/* W tej funkcji okreœlamy to co ma byc narysowane na ekranie.
* Jest wywo³ywana zawsze wtedy, gdy trzeba przerysowaæ ekran - bufor ramki.
*/
void DrawScene(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	DrawSceneAxes();
	glutSwapBuffers();

}

/* Ta funkcja jest wywo³ywana przez funkcjê DrawScene.
* Jej zadaniem jest rysowanie konkretnych obiektów osi uk³adu
* wspó³rzêdnych.
*/
void TimerFunction() {
	//x_angle=x_angle+v_angle;
	//y_angle=y_angle+v_angle;

	glutPostRedisplay();
	//glColor3f(0.5f, 0.5f, 0.5f);

	// 1 millisecond
	//glutTimerFunc(1000, TimerFunction, 0);
}


void DrawSceneAxes(void)
{
	// Definiujemy nowy typ jako tablicê 3-elementow¹
	typedef float pt3d[3];

	// Pocz¹tek i koniec osi X
	pt3d x_beg = { -10.0f, 0.0f, 0.0f };
	pt3d x_end = { 10.0f, 0.0f, 0.0f };

	// Poczatek i koniec osi Y
	pt3d y_beg = { 0.0f, -10.0f, 0.0f };
	pt3d y_end = { 0.0f,  10.0f, 0.0f };

	// Pocz¹tek i koniec osi Z
	pt3d z_beg = { 0.0f, 0.0f, -10.0f };
	pt3d z_end = { 0.0f, 0.0f,  10.0f };

	// Rysujemy osie
	glBegin(GL_LINES);
	// Czerwony kolor dla osi X
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3fv(x_beg);
	glVertex3fv(x_end);

	// Zielony kolor dla osi Y
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3fv(y_beg);
	glVertex3fv(y_end);

	// Niebieski kolor dla osi Z
	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3fv(z_beg);
	glVertex3fv(z_end);
	glEnd();

	glRotatef(x_angle, 1.0f, 0.0f, 0.0f);

	glRotatef(y_angle, 0.0f, 1.0f, 0.0f);
	glRotatef(z_angle, 0.0f, 0.0f, 1.0f);
	TimerFunction();
	//KOLOR DZBANKA
	glColor3f(0.5f, 0.5f, 0.5f);
	glutWireTeapot(4.0);
}
/* Funkcja obs³uguj¹ca klawiaturê */
void KeyboardFunc(unsigned char key, int x, int y)
{
	x_angle = 0;
	y_angle = 0;
	z_angle = 0;
	switch(key)
	{
	case 'w':
		x_angle = SPEED;
		break;
	case 's':
		x_angle = -SPEED;
		break;
	case 'a':
		y_angle = -SPEED;
		break;
	case 'd':
		y_angle = SPEED;
	case 'q':
		z_angle = -SPEED;
		break;
	case 'e':
		z_angle = SPEED;
		break;
	}

	glutPostRedisplay();
}


void InitOpenGL(void)
{
	// Ustawiamy domyœlny, czarny kolor t³a okna - bufor ramki malujemy na czarno
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
}

void ReshapeWindow(int width, int height)
{
	//printf("w: %d\n", width);
	//printf("h: %d\n", height);
	GLdouble nRange = 15.0f;
	// Na wypadek dzielenia przez 0
	if (height == 0)
	{
		height = 1;
	}

	if (width == 0)
	{
		width = 1;
	}

	// Ustawiamy wielkoœci okna urz¹dzenia w zakresie
	// od 0,0 do wysokoœæ, szerokoœæ
	glViewport(0, 0, width, height);

	// Ustawiamy uk³ad wspó³rzêdnych obserwatora
	glMatrixMode(GL_PROJECTION);

	// Resetujemy macierz projekcji 
	glLoadIdentity();

	// Korekta
	if (width <= height)
		glOrtho(-nRange, nRange, -nRange*((GLdouble)height / width), nRange*((GLdouble)height / width), -nRange, nRange);
	else
		glOrtho(-nRange*((GLdouble)width / height), nRange*((GLdouble)width / height), -nRange, nRange, -nRange, nRange);

	// Ustawiamy macierz modelu
	glMatrixMode(GL_MODELVIEW);

	// Resetujemy macierz modelu
	glLoadIdentity();
}



